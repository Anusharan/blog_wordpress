/*-----------------------------------------------------------------------------------*/
/* Twenty Minutes  WordPress Popular Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   Twenty Minutes
Theme URI       :   https://www.zylothemes.com/themes/free-wordpress-popular-theme/
Version         :   1.3.0
Tested up to    :   WP 4.9.8
Author          :   Twenty Minutes
Author URI      :   https://www.zylothemes.com/

License: GNU General Public License version 2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@zylothemes.com

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/


Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.

2 - Roboto - https://www.google.com/fonts/specimen/Roboto
	License: Distributed under the terms of the Apache License, version 2.0 		http://www.apache.org/licenses/

3 -  jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com
/*-----------------------------------------------------------------------------------*/
/* Copyright */
/*-----------------------------------------------------------------------------------*/

Twenty Minutes WordPress Theme, Copyright 2016 Zylo Themes
Twenty Minutes is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see < http://www.gnu.org/licenses/ >		

For any help you can mail us at support[at]zylothemes.com


